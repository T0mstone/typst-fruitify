# Fruitify

Make your equations more fruity!

This package automatically replaces any single letters in equations with fruit emoji.

Refer to `example-documentation.typ` for more detail.

## Emoji support

Until 0.12, typst did not have good emoji support for PDF.
This meant that even though this package worked as intended,
the output would look very wrong when exporting to PDF.
Therefore, it is recommended to stick with PNG export for those older typst versions.
