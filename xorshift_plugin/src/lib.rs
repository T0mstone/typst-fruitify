use rand_xoshiro::rand_core::{RngCore, SeedableRng};
use rand_xoshiro::Xoshiro128StarStar as Rng;

fn rand_impl(seed: [u8; 16], m: u32) -> u32 {
	let mut rng = Rng::from_seed(seed);
	rng.next_u32().checked_rem(m).unwrap_or_default()
}

fn copy_from_slice<const N: usize>(arr: &mut [u8; N], slice: &[u8]) {
	if slice.len() < arr.len() {
		arr[..slice.len()].copy_from_slice(slice);
	} else {
		arr.copy_from_slice(&slice[..N]);
	}
}

#[cfg(target_family = "wasm")]
use wasm_minimal_protocol::*;

#[cfg(target_family = "wasm")]
initiate_protocol!();

#[cfg(target_family = "wasm")]
#[wasm_func]
pub fn rand(seed_arg: &[u8], mod_arg: &[u8]) -> Vec<u8> {
	let mut seed: [u8; 16] = *b"Seed McSeedface!";
	copy_from_slice(&mut seed, seed_arg);
	let mut mod_ = [0u8; 4];
	copy_from_slice(&mut mod_, mod_arg);
	// LE is important here so that too short args stay small
	let m = u32::from_le_bytes(mod_);

	let res = crate::rand_impl(seed, m);

	res.to_le_bytes().to_vec()
}

#[cfg(test)]
mod tests {
	// Make sure to test this in release mode
	#[no_panic::no_panic]
	pub fn test_nopanic(seed: [u8; 16], m: u32) -> u32 {
		crate::rand_impl(seed, m)
	}

	#[test]
	fn test_zero_nopanic() {
		std::hint::black_box(test_nopanic([0; 16], 0));
	}
}
