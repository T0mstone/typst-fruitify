#!/usr/bin/env sh

git clone https://github.com/rust-random/rand
cd rand
git checkout 7aa25d577e2df84a5156f824077bb7f6bdf28d97
git apply ../rand_array_chunks.patch